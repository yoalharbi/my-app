import { NgModule } from "@angular/core";
import { HomeComponent } from "./home.component";
import { BrowserModule } from "@angular/platform-browser";
import { AppRoutingModule } from "../app-routing.module";
import { HomeRoutingModule } from "./home-routing.module";
import { CommonModule } from "@angular/common";
import { LoginComponent } from './login/login.component';





@NgModule({
    declarations: [
      HomeComponent,
      LoginComponent 
    ],
    imports: [
    CommonModule,
    HomeRoutingModule
    ],
    providers: [],
    bootstrap: []
  })

export class HomeModule { }