import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { LayoutComponent } from "./layout.component";
import { LangingPageComponent } from "./langing-page/langing-page.component";


const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {path: '', redirectTo: 'portal', pathMatch: 'full'},
            {
                path: 'portal',
                component: LangingPageComponent
            },
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})



export class LayoutRoutingModule { }